#Coding Exercise Instructions
Download this code
#
Install the latest version of nodejs
#
In command line switch to the root folder where the code was downloaded and type `npm install`
#
Then type `ng serve` and start a browser
#
In the browser navigate to `http://localhost:4200`
