import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { DisplayDataComponent } from './display-data/display-data.component';
import { AccessSspDataService } from './display-data/access-ssp-data.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        DisplayDataComponent
      ],
      imports: [
        HttpClientModule
      ],
      providers: [
        AccessSspDataService,
        HttpClient
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
