import { Geometry } from './geometry.model';
import { Properties } from './properties.model';

export class FeatureData {
  id: string;
  type: string;
  geometry: Geometry;
  properties: Properties;

  constructor(json: any) {
    const id: string = json.id;
    const type: string = json.type;
    let geoType: string;
    const geoCoordinates = new Array<number>();
    let properties: Properties;

    if (json.geometry) {
      geoType = json.geometry.type;
      if (json.geometry.coordinates && Array.isArray(json.geometry.coordinates)) {
        json.geometry.coordinates.forEach(coord => {
          const coordVal = Number.parseFloat(coord);
          if (!Number.isNaN(coordVal)) {
            geoCoordinates.push(Number.parseFloat(coord));
          }
        });
      }
    }
    const geometry = new Geometry(geoType, geoCoordinates);

    if (json.properties) {
      properties = new Properties(json.properties.wikipedia, json.properties.city);
    }

    this.id = id;
    this.type = type;
    this.geometry = geometry;
    this.properties = properties;
  }
}
