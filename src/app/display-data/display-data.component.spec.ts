import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayDataComponent } from './display-data.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AccessSspDataService } from './access-ssp-data.service';

describe('DisplayDataComponent', () => {
  let component: DisplayDataComponent;
  let fixture: ComponentFixture<DisplayDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayDataComponent ],
      imports: [
        HttpClientModule
      ],
      providers: [
        AccessSspDataService,
        HttpClient
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
