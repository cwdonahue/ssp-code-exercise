import { FeatureData } from './feature-data.model';

export class SspData {
  Title: string;
  Instructions: string;
  Description: string;
  FeatureData: FeatureData[];

  constructor(json: any) {
    const title: string = json.Title;
    const instructions: string = json.Instructions;
    const description: string = json.Description;
    const features = new Array<FeatureData>();
    if (json.FeatureData && Array.isArray(json.FeatureData)) {
      json.FeatureData.forEach(feature => {
        features.push(new FeatureData(feature));
      });
    }

    this.Title = title;
    this.Instructions = instructions;
    this.Description = description;
    this.FeatureData = features;
  }
}
