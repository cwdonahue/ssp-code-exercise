export class Properties {
  wikipedia: string;
  city: string;

  constructor(wikipedia: string, city: string) {
    this.wikipedia = wikipedia;
    this.city = city;
  }
}
