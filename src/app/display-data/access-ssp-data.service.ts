import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SspData } from './ssp-data.model';

@Injectable()
export class AccessSspDataService {
  http: HttpClient;

  constructor(private httpClient: HttpClient) {
    this.http = httpClient;
  }

  public getSSPData(): Promise<SspData> {
    const url = 'http://sspcodingexercise.s3-website-us-west-2.amazonaws.com/Sample.json';
    return new Promise(resolve => {
      this.httpClient.get(url).subscribe(data => {
        resolve(new SspData(data));
      }, error => {
        console.log(error);
      });
    });
  }
}
