import { Component } from '@angular/core';
import { AccessSspDataService } from './access-ssp-data.service';
import { SspData } from './ssp-data.model';

@Component({
  selector: 'app-display-data',
  templateUrl: './display-data.component.html',
  styleUrls: ['./display-data.component.scss']
})
export class DisplayDataComponent {
  dataService: AccessSspDataService;
  dataReady = false;
  sspData: SspData;

  testData = JSON.parse(`{
      "Title": "SSP Innovations Sample Code",
      "Instructions": "",
      "Description": "The following data contains summary and geographical information from Hawaii and Anchorage",
      "FeatureData" : [{
      "geometry": {
        "type": "Point",
        "coordinates": [-157.817, 21.3]
      },
      "type": "Feature",
      "properties": {
        "wikipedia": "Honolulu",
        "city": "Honolulu"
      },
      "id": "Honolulu"
    }, {
      "geometry": {
        "type": "Point",
        "coordinates": [-155.1, 19.7]
      },
      "type": "Feature",
      "properties": {
        "wikipedia": "Hilo,_Hawaii",
        "city": "Hilo"
      },
      "id": "Hilo"
    }, {
      "geometry": {
        "type": "Point",
        "coordinates": [-149.883, 61.217]
      },
      "type": "Feature",
      "properties": {
        "wikipedia": "Anchorage,_Alaska",
        "city": "Anchorage"
      },
      "id": "Anchorage"
    }]}`);

  constructor(service: AccessSspDataService) {
    this.dataService = service;
   }

  public GrabData() {
    this.dataService.getSSPData()
    .then((results) => {
      this.dataReady = true;
      this.sspData = results;
    })
    .catch((error) => {
      console.log(error);
    });
  }

}
