import { SspData } from './ssp-data.model';

describe('SSPData', () => {
  const goodData = `{
    "Title": "Test Title",
    "Instructions": "Test Instructions",
    "Description": "Test Description",
    "FeatureData" : [{
      "geometry": {
        "type": "GeoType",
        "coordinates": [100, 90]
      },
      "type": "Feature Type",
      "properties": {
        "wikipedia": "Test Wiki",
        "city": "Test City"
      },
      "id": "Test1"
    }]
  }`;

  const badFeatureData = `{
    "Title": "Test Title",
    "Instructions": "Test Instructions",
    "Description": "Test Description",
    "FeatureData" : {}
  }`;

  const badCoordData = `{
    "Title": "Test Title",
    "Instructions": "Test Instructions",
    "Description": "Test Description",
    "FeatureData" : [{
      "geometry": {
        "type": "GeoType",
        "coordinates": ["X", "Y"]
      },
      "type": "Feature Type",
      "properties": {
        "wikipedia": "Test Wiki",
        "city": "Test City"
      },
      "id": "Test1"
    }]
  }`;

  it('should map json object', () => {
    const object = JSON.parse(goodData);
    const sspData = new SspData(object);

    expect(sspData).not.toBeNull();
    expect(sspData.Title).toEqual('Test Title');
    expect(sspData.FeatureData.length).toEqual(1);
  });

  it('should handle bad feature data - not array', () => {
    const object = JSON.parse(badFeatureData);
    const sspData = new SspData(object);

    expect(sspData).not.toBeNull();
    expect(sspData.FeatureData.length).toEqual(0);
  });

  it('should handle bad coordinate data - not array', () => {
    const object = JSON.parse(badCoordData);
    const sspData = new SspData(object);

    expect(sspData).not.toBeNull();
    expect(sspData.FeatureData.length).toEqual(1);
    expect(sspData.FeatureData[0].geometry).not.toBeNull();
    expect(sspData.FeatureData[0].geometry.coordinates.length).toEqual(0);
  });
});
