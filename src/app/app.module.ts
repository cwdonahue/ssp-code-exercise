import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DisplayDataComponent } from './display-data/display-data.component';
import { AccessSspDataService } from './display-data/access-ssp-data.service';

@NgModule({
  declarations: [
    AppComponent,
    DisplayDataComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    AccessSspDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
